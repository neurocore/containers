#consumer.py
from kafka import KafkaConsumer

#connect to Kafka server and pass topic we want to consume
consumer=KafkaConsumer('get-new-files', group_id='header',
			bootstrap_servers=['kafka:9092'])
#continuously listen to the connection and print messages as recieved

from sqlalchemy import create_engine
engine = create_engine("mysql://root:neurocoreai@containers_aidb_1/production")
connection =engine.connect()
connection.execute('create table if not exists `header_data` ( `target_id` double, `target_name` varchar(200), `center_latitude` double, `center_longitude` double, `center_altitude` double, `timestamp` datetime, `filename` varchar(100))')
import datetime
from os import path
import img_header
import pandas as pd

for msg in consumer:
	filefound=path.split(msg.value)
	curtimestamp=datetime.datetime.today()
	#run our function to create fake header data
	#will want to replace this with the real header data
	header_data= img_header.fake_header()
	#add the filename for our database
	header_data['filename']=pd.Series(filefound[1])
	#add this to our header_data table in database
	header_data.to_sql(con=engine, name='header_data', if_exists='append', index=False)
	print(header_data)


