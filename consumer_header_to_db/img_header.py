#this function will adapt for our clients' data formats. 
#may need to be custom for each client/data type
def get_tif_header(img_filename):
	import exifread
	path_name= "/images/" +img_filename
	## Open image file for reading (binary mode)
	f = open(path_name, 'rb')
	# Return Exif tags
	tags = exifread.process_file(f)
	this= tags.items()
	# tags.keys()# that only got some generic info, no coords
	return(this)

	
#this function will return a random set of metadata for an image
#this takes in no parameters since it is just generating a city and coords
def fake_header():
	import numpy as np
	from numpy import random
	import pandas as pd
	from geopy.geocoders import Nominatim
	import datetime
	geolocator = Nominatim()

	targets= ["Kolkata, India", "Guangzhou, China", "Hyderabad, India", "Tianjin, China", "Hanoi, Vietnam", "Beijing, China", "Yokohama, Japan", "Surat, India", "Karachi, Pakistan", "Berlin, Germany","Baghdad, Iraq", "Seoul, South Korea", "Abidjan, Ivory Coast", "Chennai, India", "Jakarta, Indonesia", "Singapore", "Addis Ababa, Ethiopia", "Johannesburg, South Africa", "Yangon, Myanmar", "Moscow, Russia", "Mexico City, Mexico", "Nairobi, Kenya"]

	#want LLA and city name as target
	target_id = random.randint(0,targets.__len__())
	target_name = targets[target_id]
	#get coordinates of this city
	location = geolocator.geocode(target_name)
	#create my dataframe with coordinates
	this=[{'target_id':target_id,'target_name': target_name,'center_latitude': float(location.latitude + random.rand(1) * .5),'center_longitude': float(location.longitude + random.rand(1) * .5),'center_altitude': float(location.altitude + random.rand(1) * .5),'timestamp': datetime.datetime.today()}]
	header_data=pd.DataFrame(this)
	return(header_data)