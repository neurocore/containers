#consumer.py
from kafka import KafkaConsumer

#connect to Kafka server and pass topic we want to consume
consumer=KafkaConsumer('get-new-files', group_id='new',
			bootstrap_servers=['kafka:9092'])
#continuously listen to the connection and print messages as recieved

from sqlalchemy import create_engine
engine = create_engine("mysql://root:neurocoreai@containers_aidb_1/production")
connection =engine.connect()
connection.execute('create table if not exists `img_file` (`timestamp` datetime, `filename` varchar(100))')
import datetime
from os import path
for msg in consumer:
	filefound=path.split(msg.value)
	curtimestamp=datetime.datetime.today()
	insertstatement= 'insert into img_file (timestamp, filename) VALUES ("' + curtimestamp.isoformat() + '", "' + filefound[-1] + '")'
	connection.execute(insertstatement)
	print(insertstatement)
