from kafka import KafkaConsumer
consumer=KafkaConsumer('noise_bot_out', group_id='to_database',
			bootstrap_servers=['kafka:9092'])

from sqlalchemy import create_engine
engine = create_engine("mysql://root:neurocoreai@containers_aidb_1/production")
connection =engine.connect()
connection.execute('create table if not exists `noise_robot_out` (`bot_out_timestamp` datetime, `filename` varchar(100),`st_dev` double)')
import datetime
from os import path
for msg in consumer:
	#msg no longer comes in as a comma separated string
	file = path.split(msg.key)
	msg_timestamp=datetime.datetime.fromtimestamp(msg.timestamp/1000)
	insertstatement= 'insert into noise_robot_out (bot_out_timestamp, filename, st_dev) VALUES ("' + msg_timestamp.isoformat() + '", "' + file[-1] + '", "' + msg.value + '")'
	connection.execute(insertstatement)
	print(insertstatement)