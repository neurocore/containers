FROM ubuntu:16.04
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y ubuntu-server



SHELL ["/bin/bash", "-c"]

RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    gcc \
    bzip2 \
    libvips \
    libvips-dev \
    libvips-tools \
    openssh-server \
    mc  

RUN echo 'net.ipv6.conf.all.disable_ipv6 = 1' >> /etc/sysctl.conf && \
    echo 'net.ipv6.conf.default.disable_ipv6 = 1' >> /etc/sysctl.conf && \
    echo 'net.ipv6.conf.lo.disable_ipv6 = 1' >> /etc/sysctl.conf && \
    service networking restart

ENV PATH $PATH:/portal/conda/bin

# Install miniconda

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-4.6.14-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /portal/conda && \
    rm ~/miniconda.sh && \
    /portal/conda/bin/conda clean -tipsy && \
    ln -s /portal/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /portal/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate tf_1.13_py_3.6" >> ~/.bashrc

#  Create conda environment with 3.6 (tensorflow can't run on 3.7)
COPY tf1.13_py3.6.yml ./
RUN conda env create -f tf1.13_py3.6.yml

RUN service ssh start 

WORKDIR /

ADD set_root_pw.sh /set_root_pw.sh

RUN chmod a+rwx /*.sh && \
    bash -c "/set_root_pw.sh" && \
    rm -f /set_root_pw.sh

ADD authorized_keys /home/chris/.ssh/authorized_keys
ADD authorized_keys /home/xandria/.ssh/authorized_keys
ADD authorized_keys /home/robot/.ssh/authorized_keys

RUN chown -R chris:chris /home/chris && \
    chown -R xandria:xandria /home/xandria && \
    chown -R robot:robot /home/robot && \
    chown -R robot:robot /portal && \
    chmod 770 -R /portal && \
    echo ". /portal/conda/etc/profile.d/conda.sh" >> home/chris/.bashrc && \
    echo "conda activate tf_1.13_py_3.6" >> home/chris/.bashrc && \
    echo ". /portal/conda/etc/profile.d/conda.sh" >> home/xandria/.bashrc && \
    echo "conda activate tf_1.13_py_3.6" >> home/xandria/.bashrc


EXPOSE 8888
EXPOSE 6006
EXPOSE 22

CMD ["bash", "-c", " /usr/sbin/sshd -D & /bin/bash"]

