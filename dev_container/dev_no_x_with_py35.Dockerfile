# Copyright 2018 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# THIS IS A GENERATED DOCKERFILE.
#
# This file was assembled from multiple pieces, whose use is documented
# below. Please refer to the the TensorFlow dockerfiles documentation for
# more information. Build args are documented as their default value.
#
# Ubuntu-based, Nvidia-GPU-enabled environment for developing changes for TensorFlow, with Jupyter included.
#
# Start from Nvidia's Ubuntu base image with CUDA and CuDNN, with TF development
# packages.
# --build-arg UBUNTU_VERSION=16.04
#    ( no description )
#
# Python is required for TensorFlow and other libraries.
# --build-arg USE_PYTHON_3_NOT_2=True
#    Install python 3 over Python 2
#
# Install the latest version of Bazel and Python development tools.
#
# Configure TensorFlow's shell prompt and login tools.
#
# Launch Jupyter on execution instead of a bash prompt.

ARG UBUNTU_VERSION=16.04
FROM nvidia/cuda:9.0-base-ubuntu${UBUNTU_VERSION}

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential 
RUN apt-get update && apt-get install -y --no-install-recommends \
        cuda-command-line-tools-9-0 \
        cuda-cublas-dev-9-0 \
        cuda-cudart-dev-9-0 \
        cuda-cufft-dev-9-0 \
        cuda-curand-dev-9-0 \
        cuda-cusolver-dev-9-0 \
        cuda-cusparse-dev-9-0 \
        curl \
        git \
        libcudnn7=7.2.1.38-1+cuda9.0 \
        libcudnn7-dev=7.2.1.38-1+cuda9.0 \
        libnccl2=2.2.13-1+cuda9.0 \
        libnccl-dev=2.2.13-1+cuda9.0 \
        libcurl3-dev \
        libfreetype6-dev \
        libhdf5-serial-dev \
        libpng12-dev \
        libzmq3-dev \
        pkg-config \
        rsync \
        software-properties-common \
        unzip \
        zip \
        zlib1g-dev \
        wget \
        openssh-server \
        nodejs \
        sudo \
        vim \
        mc \
        protobuf-compiler \
        openjdk-8-jdk \
        swig \
        npm \
        openssh-server \
        xubuntu-core^ \
        && \
    rm -rf /var/lib/apt/lists/* && \
    find /usr/local/cuda-9.0/lib64/ -type f -name 'lib*_static.a' -not -name 'libcudart_static.a' -delete && \
    rm /usr/lib/x86_64-linux-gnu/libcudnn_static_v7.a



RUN apt-get update && \
        apt-get install -y nvinfer-runtime-trt-repo-ubuntu1604-4.0.1-ga-cuda9.0 && \
        apt-get update && \
        apt-get install -y libnvinfer4=4.1.2-1+cuda9.0 && \
        apt-get install -y libnvinfer-dev=4.1.2-1+cuda9.0

# Link NCCL libray and header where the build script expects them.
RUN mkdir /usr/local/cuda-9.0/lib &&  \
    ln -s /usr/lib/x86_64-linux-gnu/libnccl.so.2 /usr/local/cuda/lib/libnccl.so.2 && \
    ln -s /usr/include/nccl.h /usr/local/cuda/include/nccl.h

# TODO(tobyboyd): Remove after license is excluded from BUILD file.
RUN gunzip /usr/share/doc/libnccl2/NCCL-SLA.txt.gz && \
    cp /usr/share/doc/libnccl2/NCCL-SLA.txt /usr/local/cuda/

# Install bazel
RUN echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list && \
    curl https://bazel.build/bazel-release.pub.gpg | apt-key add - && \
    apt-get update && \
    apt-get install -y bazel

COPY bashrc /etc/bash.bashrc
RUN chmod a+rwx /etc/bash.bashrc

RUN mkdir /notebooks && chmod a+rwx /notebooks && \
    mkdir /.local && chmod a+rwx /.local && \
    mkdir /portal && \
    mkdir /transfer && \
    service ssh restart 


RUN echo 'net.ipv6.conf.all.disable_ipv6 = 1' >> /etc/sysctl.conf && \
    echo 'net.ipv6.conf.default.disable_ipv6 = 1' >> /etc/sysctl.conf && \
    echo 'net.ipv6.conf.lo.disable_ipv6 = 1' >> /etc/sysctl.conf && \
    service networking restart

SHELL ["/bin/bash", "-c"]


ENV PATH $PATH:/portal/conda/bin

RUN wget --quiet https://repo.anaconda.com/archive/Anaconda3-5.3.1-Linux-x86_64.sh -O ~/anaconda.sh && \
    /bin/bash ~/anaconda.sh -b -p /portal/conda && \
    rm ~/anaconda.sh && \
    ln -s /portal/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /portal/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc

RUN source /portal/conda/etc/profile.d/conda.sh && \
    conda activate base && \
    conda clean --all && \
    conda update --all 

RUN conda install --update-all -c anaconda orange3=3.17.0 

# RUN source /portal/conda/etc/profile.d/conda.sh && \
#     conda activate base && \
#     conda clean --all && \
#     conda update --all && \
#     conda install --update-all -c astropy glueviz 
RUN conda install --update-all -c glueviz glue-geospatial 
RUN conda install --update-all -c anaconda nodejs


RUN apt-get update && \
    apt-get install -y libvips libvips-dev libvips-tools && \
    conda install pip &&\
    pip install --upgrade \
        pyvips \
        rasterio \
        nose \
        pillow \
        jupyterlab \
        jupyter_contrib_nbextensions && \
        jupyter contrib nbextension install --sys-prefix

RUN conda create --name tensorflow_3.6 anaconda python=3.6 && \
    source /portal/conda/etc/profile.d/conda.sh && \
    conda activate tensorflow_3.6 && \
    apt-get update && \
    apt-get install -y libvips libvips-dev libvips-tools &&\
    conda install pip &&\
    conda install --update-all -y -c anaconda nodejs \
        rasterio \
        tensorflow-gpu \
        tensorboard \
        keras \
        nose \
        pillow \
        jupyterlab && \
    conda install -c conda-forge jupyter_contrib_nbextensions \
        tensorflow-probability \
        tensorflow-hub && \
    conda install --update-all --channel https://conda.anaconda.org/menpo opencv3 && \
    pip install pyvips

RUN source /portal/conda/etc/profile.d/conda.sh && \
    conda activate tensorflow_3.6 && \
    conda install -y -c conda-forge black yapf autopep8 itkwidgets core-js nodejs && \
    pip install jupyterlab_latex && \
    jupyter labextension install @jupyterlab/latex && \
    jupyter labextension install @mflevine/jupyterlab_html && \
    jupyter labextension install jupyterlab-drawio && \
    jupyter labextension install @jupyterlab/plotly-extension && \
    jupyter labextension install jupyterlab_bokeh && \
    jupyter labextension install @jupyterlab/toc && \
    jupyter labextension install @krassowski/jupyterlab_go_to_definition && \
    jupyter labextension install @ryantam626/jupyterlab_code_formatter && \
    pip install jupyterlab_code_formatter && \
    jupyter serverextension enable --py jupyterlab_code_formatter && \
    jupyter labextension install @lckr/jupyterlab_variableinspector && \
    jupyter labextension install @jupyter-widgets/jupyterlab-manager itk-jupyter-widgets && \
    jupyter labextension install @ijmbarr/jupyterlab_spellchecker && \
    jupyter labextension install jupyterlab-python-file



RUN conda create --name tensorflow_3.5 anaconda python=3.5 && \
    source /portal/conda/etc/profile.d/conda.sh && \
    conda activate tensorflow_3.5 && \
    apt-get update && \
    apt-get install -y libvips libvips-dev libvips-tools &&\
    conda install pip

RUN conda install --update-all -y -c anaconda nodejs \
        rasterio \
        tensorflow-gpu \
        tensorboard \
        keras \
        nose \
        pillow \
        jupyterlab 
RUN conda install -c conda-forge \
        tensorflow-probability \
        tensorflow-hub && \
    pip install pyvips




# RUN pip install --upgrade \
#         Orange3-Geo \
#         Orange3-ImageAnalytics \
#         Orange3-Network \
#         Orange-Spectroscopy \
#         Orange3-Text \
#         Orange3-Textable \
#         Orange3-Timeseries \

#RUN useradd -ms /bin/bash -p "$(openssl passwd -1 Beep3141Beep)" admin && \
#RUN usermod -aG sudo admin 
#RUN apt-get update && \
#    apt-get install -y freetds-dev &&\
#    pip install pymssql



RUN service ssh start 

EXPOSE 8888
EXPOSE 6006

WORKDIR /

#RUN rm -rf /portal/*


#CMD ["bash", "-c", " /usr/sbin/sshd -D & source /etc/bash.bashrc && jupyter lab --notebook-dir=/ --ip 0.0.0.0 --no-browser --allow-root"]
#RUN cd /labelImg && wget  https://www.dropbox.com/s/bpbo3oh0zgl0522/linux_v1.4.3.zip?dl=1
CMD ["bash", "-c", " /usr/sbin/sshd -D & /bin/bash"]

