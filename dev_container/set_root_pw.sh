#!/bin/bash

if [ -f /.root_pw_set ]; then
	    echo "Root password already set!"
	        exit 0
fi

#PASS=${ROOT_PASS:-$(pwgen -s 12 1)}
#PASS=${ROOT_PASS:-Beep2997Beep}
#_word=$( [ ${ROOT_PASS} ] && echo "preset" || echo "random" )
PASS=Beep2997Beep
echo "=> Setting a ${_word} password to the root user"
echo "root:$PASS" | chpasswd

adduser --disabled-password --gecos "" robot
adduser robot sudo
#DPASS=$(pwgen -s 12 1)
DPASS=Beep3141Beep
echo "=> Setting a password to the docker user"
echo "robot:$DPASS" | chpasswd

adduser --disabled-password --gecos "" chris 
adduser chris sudo
adduser chris robot
mkdir /home/chris/.ssh

adduser --disabled-password --gecos "" xandria 
adduser xandria sudo
adduser xandria robot
mkdir /home/xandria/.ssh





echo "=> Done!"
touch /.root_pw_set


