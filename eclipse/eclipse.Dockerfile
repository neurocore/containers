FROM ubuntu:latest

MAINTAINER neurocoreai
# based on DockerX2GO by paimpozhil https://github.com/paimpozhil/DockerX2go

RUN apt-get update --no-install-recommends 

ENV DEBIAN_FRONTEND noninteractive

ADD set_root_pw.sh /set_root_pw.sh
ADD run.sh /run.sh

RUN apt-get install -y --no-install-recommends openssh-server xubuntu-core^ && \
    add-apt-repository ppa:x2go/stable && \
    apt-get update && \
	apt-get install --no-install-recommends x2goserver x2goserver-xsession pwgen -y && \
	mkdir -p /var/run/sshd
	
RUN sed -i "s/UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config && \
	sed -i "s/UsePAM.*/UsePAM no/g" /etc/ssh/sshd_config && \
	sed -i "s/#PermitRootLogin.*/PermitRootLogin yes/g" /etc/ssh/sshd_config && \
	sed -i "s/#PasswordAuthentication/PasswordAuthentication/g" /etc/ssh/sshd_config && \
	mkdir -p /tmp/.X11-unix && chmod 1777 /tmp/.X11-unix && \
	chmod a+rwx /*.sh && \
	mkdir /portal && \
    /set_root_pw.sh && \
    sed -i.bak '/mesg/c\tty -s \&\& mesg n' /root/.profile && \
    sed -i.bak '/mesg/c\tty -s \&\& mesg n' /home/admin/.profile


RUN chmod +x /*.sh 
		

EXPOSE 22

CMD ["/run.sh"]