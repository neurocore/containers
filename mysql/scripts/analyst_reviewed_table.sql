create table if not exists `analyst_reviewed`
                       (`timestamp` datetime, `filename` varchar(100), `reviewed_flag` bool,
                    `analyst_name` varchar(100), `notes` varchar(400));