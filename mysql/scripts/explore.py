# import exifread
#
# path_name= "D:\Pictures\TC_NG_SFBay_US_Geo.tif"
#
# ## Open image file for reading (binary mode)
# f = open(path_name, 'rb')
#
# # Return Exif tags
# tags = exifread.process_file(f)
# tags.items()
# tags.keys()
# # that only got some generic info, no coords
#
# from osgeo import gdal, ogr, osr
# #some functions to look at: https://www.programcreek.com/python/example/58591/osgeo.osr.SpatialReference
# dataset = gdal.Open(path_name,gdal.GA_ReadOnly )
# coords= dataset.GetGeoTransform()
# long = coords[0]
# lat = coords[3]
# alt = coords[1]
# print("Lat: {} Long: {} Alt: {}" .format(lat, long, alt))


import numpy as np
from numpy import random
import pandas as pd
from geopy.geocoders import Nominatim
from random import randrange
import datetime

N= 40 # how many images do we have?
hours =24

geolocator = Nominatim()

locations = pd.DataFrame(columns=['target_name', 'target_id','target_latitude','target_longitude', 'target_altitude',
                                  'center_latitude', 'center_longitude', 'center_altitude', 'timestamp'])
targets= ["Kolkata, India", "Guangzhou, China", "Hyderabad, India", "Tianjin, China", "Hanoi, Vietnam",
          "Beijing, China", "Yokohama, Japan", "Surat, India", "Karachi, Pakistan", "Berlin, Germany",
          "Baghdad, Iraq", "Seoul, South Korea", "Abidjan, Ivory Coast", "Chennai, India", "Jakarta, Indonesia",
          "Singapore", "Addis Ababa, Ethiopia", "Johannesburg, South Africa", "Yangon, Myanmar", "Moscow, Russia",
          "Mexico City, Mexico", "Nairobi, Kenya"]

#create my empty dataframe with coordinates
#want LLA and city name as target
for i in range(0,N):
    target_id = random.randint(0,targets.__len__())
    target_name = targets[target_id]
    location = geolocator.geocode(target_name)
    locations.loc[i]= {'target_id':target_id,
                      'target_name': target_name,
                      'target_latitude': float(location.latitude),
                      'target_longitude': float(location.longitude),
                      'target_altitude': float(location.altitude),
                      'center_latitude': float(location.latitude + random.rand(1) * .5),
                      'center_longitude': float(location.longitude + random.rand(1) * .5),
                      'center_altitude': float(location.altitude + random.rand(1) * .5),
                       'timestamp': datetime.datetime.today()- datetime.timedelta(seconds=randrange(3600*hours))}#generates timestamps over the last day

#note that the timestamps are not in order, but it doesn't really matter for our purposes for now.
#  we can sort when we display
for i in range(0,N):
    t= locations.loc[i,'timestamp']
    time_stripped = datetime.date.strftime(t, '%Y%M%d%H%M%S%f')
    locations.loc[i,'filename']= 'EO_' + str(locations.loc[i,'target_id']) + '_' + time_stripped

locations['img_qual_rating'] = random.uniform(1, 10, N)

from sqlalchemy import create_engine
import pymysql
engine = create_engine("mysql://root:neurocoreai@dockerfiles_aidb_1/demodb")
locations.to_sql(con=engine, name='img_metadata', if_exists='replace')

#locations.head()
from imagesSave import *