create view latest_review as
select filename, max(timestamp) as maxtime
from analyst_reviewed group by filename;

create view newest_analyst_reviewed as 
select r.filename, r.analyst_name, r.timestamp, r.reviewed_flag
from latest_review
inner join analyst_reviewed as r
on r.filename=latest_review.filename 
and 
r.timestamp = latest_review.maxtime;