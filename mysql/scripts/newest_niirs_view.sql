#two separate views:
create view latest_preds as
select filename, max(timestamp) as maxtime
from predicted_NIIRS group by filename;


create view newest_predicted_NIIRS as 
select p.filename, p.predicted_NIIRS, p.timestamp
from latest_preds
inner join predicted_NIIRS as p 
on p.filename=latest_preds.filename 
and 
p.timestamp = latest_preds.maxtime;