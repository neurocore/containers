create view out_of_bounds as 
Select n.filename ,  
case 
when n.uncertainty>0.2 then 'uncertainty over 0.2' 
when abs(n.NIIRS-p.predicted_NIIRS)>.1 then 'delta over .1'
else NULL
end as reason,
case when n.uncertainty>0.2 or abs(n.NIIRS-p.predicted_NIIRS)>.1 then 1
else 0
end as flag ,
case when r.reviewed_flag =1 then 'reviewed'
else 'unreviewed'
end as status 
from NIIRS_robot_out n
left join predicted_NIIRS p
on n.filename=p.filename
left join newest_analyst_reviewed r 
on n.filename = r.filename;