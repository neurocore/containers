-- MySQL dump 10.13  Distrib 5.7.15, for Linux (x86_64)
--
-- Host: localhost    Database: production
-- ------------------------------------------------------
-- Server version	5.7.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE DATABASE IF NOT EXISTS production;
USE production;
--
-- Table structure for table `NIIRS_robot_out`
--

DROP TABLE IF EXISTS `NIIRS_robot_out`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NIIRS_robot_out` (
  `index` bigint(20) DEFAULT NULL,
  `bot_out_timestamp` datetime DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `NIIRS` double DEFAULT NULL,
  `uncertainty` double DEFAULT NULL,
  KEY `ix_NIIRS_robot_out_index` (`index`),
  KEY `filename_index` (`filename`),
  KEY `time_index` (`bot_out_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NIIRS_robot_out`
--

LOCK TABLES `NIIRS_robot_out` WRITE;
/*!40000 ALTER TABLE `NIIRS_robot_out` DISABLE KEYS */;
/*!40000 ALTER TABLE `NIIRS_robot_out` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `analyst_NIIRS`
--

DROP TABLE IF EXISTS `analyst_NIIRS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyst_NIIRS` (
  `timestamp` datetime DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `analyst_NIIRS` double DEFAULT NULL,
  `analyst_name` varchar(100) DEFAULT NULL,
  KEY `filename_index` (`filename`),
  KEY `an_timestamp_index` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `analyst_NIIRS`
--

LOCK TABLES `analyst_NIIRS` WRITE;
/*!40000 ALTER TABLE `analyst_NIIRS` DISABLE KEYS */;
/*!40000 ALTER TABLE `analyst_NIIRS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `analyst_reviewed`
--

DROP TABLE IF EXISTS `analyst_reviewed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analyst_reviewed` (
  `timestamp` datetime DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `reviewed_flag` tinyint(1) DEFAULT NULL,
  `analyst_name` varchar(100) DEFAULT NULL,
  `notes` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `analyst_reviewed`
--

LOCK TABLES `analyst_reviewed` WRITE;
/*!40000 ALTER TABLE `analyst_reviewed` DISABLE KEYS */;
/*!40000 ALTER TABLE `analyst_reviewed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `header_data`
--

DROP TABLE IF EXISTS `header_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `header_data` (
  `index` bigint(20) DEFAULT NULL,
  `target_name` varchar(100) DEFAULT NULL,
  `target_id` bigint(20) DEFAULT NULL,
  `center_latitude` double DEFAULT NULL,
  `center_longitude` double DEFAULT NULL,
  `center_altitude` double DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  KEY `ix_header_data_index` (`index`),
  KEY `filename_index` (`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `header_data`
--

LOCK TABLES `header_data` WRITE;
/*!40000 ALTER TABLE `header_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `header_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `img_file`
--

DROP TABLE IF EXISTS `img_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `img_file` (
  `timestamp` datetime DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `img_file`
--

LOCK TABLES `img_file` WRITE;
/*!40000 ALTER TABLE `img_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `img_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `latest_preds`
--

DROP TABLE IF EXISTS `latest_preds`;
/*!50001 DROP VIEW IF EXISTS `latest_preds`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `latest_preds` AS SELECT 
 1 AS `filename`,
 1 AS `maxtime`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `latest_review`
--

DROP TABLE IF EXISTS `latest_review`;
/*!50001 DROP VIEW IF EXISTS `latest_review`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `latest_review` AS SELECT 
 1 AS `filename`,
 1 AS `maxtime`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `newest_analyst_reviewed`
--

DROP TABLE IF EXISTS `newest_analyst_reviewed`;
/*!50001 DROP VIEW IF EXISTS `newest_analyst_reviewed`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `newest_analyst_reviewed` AS SELECT 
 1 AS `filename`,
 1 AS `analyst_name`,
 1 AS `timestamp`,
 1 AS `reviewed_flag`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `newest_predicted_NIIRS`
--

DROP TABLE IF EXISTS `newest_predicted_NIIRS`;
/*!50001 DROP VIEW IF EXISTS `newest_predicted_NIIRS`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `newest_predicted_NIIRS` AS SELECT 
 1 AS `filename`,
 1 AS `predicted_NIIRS`,
 1 AS `timestamp`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `noise_robot_out`
--

DROP TABLE IF EXISTS `noise_robot_out`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noise_robot_out` (
  `bot_out_timestamp` datetime DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `st_dev` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noise_robot_out`
--

LOCK TABLES `noise_robot_out` WRITE;
/*!40000 ALTER TABLE `noise_robot_out` DISABLE KEYS */;
/*!40000 ALTER TABLE `noise_robot_out` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `out_of_bounds`
--

DROP TABLE IF EXISTS `out_of_bounds`;
/*!50001 DROP VIEW IF EXISTS `out_of_bounds`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `out_of_bounds` AS SELECT 
 1 AS `filename`,
 1 AS `reason`,
 1 AS `flag`,
 1 AS `status`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `predicted_NIIRS`
--

DROP TABLE IF EXISTS `predicted_NIIRS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `predicted_NIIRS` (
  `timestamp` datetime DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `predicted_NIIRS` double DEFAULT NULL,
  `analyst_name` varchar(100) DEFAULT NULL,
  KEY `filename_index` (`filename`),
  KEY `timestamp_index` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predicted_NIIRS`
--

LOCK TABLES `predicted_NIIRS` WRITE;
/*!40000 ALTER TABLE `predicted_NIIRS` DISABLE KEYS */;
/*!40000 ALTER TABLE `predicted_NIIRS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `latest_preds`
--

/*!50001 DROP VIEW IF EXISTS `latest_preds`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `latest_preds` AS select `predicted_NIIRS`.`filename` AS `filename`,max(`predicted_NIIRS`.`timestamp`) AS `maxtime` from `predicted_NIIRS` group by `predicted_NIIRS`.`filename` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `latest_review`
--

/*!50001 DROP VIEW IF EXISTS `latest_review`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `latest_review` AS select `analyst_reviewed`.`filename` AS `filename`,max(`analyst_reviewed`.`timestamp`) AS `maxtime` from `analyst_reviewed` group by `analyst_reviewed`.`filename` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `newest_analyst_reviewed`
--

/*!50001 DROP VIEW IF EXISTS `newest_analyst_reviewed`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `newest_analyst_reviewed` AS select `r`.`filename` AS `filename`,`r`.`analyst_name` AS `analyst_name`,`r`.`timestamp` AS `timestamp`,`r`.`reviewed_flag` AS `reviewed_flag` from (`latest_review` join `analyst_reviewed` `r` on(((`r`.`filename` = `latest_review`.`filename`) and (`r`.`timestamp` = `latest_review`.`maxtime`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `newest_predicted_NIIRS`
--

/*!50001 DROP VIEW IF EXISTS `newest_predicted_NIIRS`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `newest_predicted_NIIRS` AS select `p`.`filename` AS `filename`,`p`.`predicted_NIIRS` AS `predicted_NIIRS`,`p`.`timestamp` AS `timestamp` from (`latest_preds` join `predicted_NIIRS` `p` on(((`p`.`filename` = `latest_preds`.`filename`) and (`p`.`timestamp` = `latest_preds`.`maxtime`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `out_of_bounds`
--

/*!50001 DROP VIEW IF EXISTS `out_of_bounds`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `out_of_bounds` AS select `n`.`filename` AS `filename`,(case when (`n`.`uncertainty` > 0.2) then 'uncertainty over 0.2' when (abs((`n`.`NIIRS` - `p`.`predicted_NIIRS`)) > 0.1) then 'delta over .1' else NULL end) AS `reason`,(case when ((`n`.`uncertainty` > 0.2) or (abs((`n`.`NIIRS` - `p`.`predicted_NIIRS`)) > 0.1)) then 1 else 0 end) AS `flag`,(case when (`r`.`reviewed_flag` = 1) then 'reviewed' else 'unreviewed' end) AS `status` from ((`NIIRS_robot_out` `n` left join `predicted_NIIRS` `p` on((`n`.`filename` = `p`.`filename`))) left join `newest_analyst_reviewed` `r` on((`n`.`filename` = `r`.`filename`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-15 19:24:09
