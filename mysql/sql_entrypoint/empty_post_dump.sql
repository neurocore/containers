### Need to analyze some tables to improve performance

use production;
analyze table NIIRS_robot_out;
analyze table analyst_NIIRS;
analyze table analyst_reviewed;
analyze table header_data;
analyze table predicted_NIIRS;
