#consumer to get images and run them through noise_robot
#this will use docker to do it so that everything is isolated
#consumer.py
from kafka import KafkaConsumer, KafkaProducer
#connect to Kafka server and pass topic we want to consume
consumer=KafkaConsumer('get-new-files', group_id='noise_robot',
			bootstrap_servers=['kafka:9092'])
#continuously listen to the connection and print messages as recieved
producer =KafkaProducer(bootstrap_servers=['kafkazookeeper_kafka_1:9092'])
import noise_predict_robot

for msg in consumer:
	filefound=msg.value
	print(filefound.decode('utf-8'))
	try:
		stdev= noise_predict_robot.main(filefound.decode('utf-8'))
	except Exception:
		producer.send(topic="error", value= b"noise_bot_fail", key=filefound)
		print("the noise robot failed to run on this image")
		print(e)
		continue
	else:
		print(str(stdev))
		producer.send(topic="noise_bot_out", value=str(stdev).encode('utf-8'), key = filefound) 
	finally:
		print("leaving noise robot consumer-producer")
