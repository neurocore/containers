# noise_predict_robot.py

########################################################################################################################
########################################################################################################################
##
## Imports and Backend
##
########################################################################################################################
########################################################################################################################

# General Imports
import argparse
import sys
import os
import glob
import numpy as np
# Keras Imports
from keras.models import load_model as load_keras_model
from keras.preprocessing.image import img_to_array, load_img

# disable TF debugging info
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'



########################################################################################################################
########################################################################################################################
##
## Inputs and Outputs
##
########################################################################################################################
########################################################################################################################


model_filename = 'denoiser_robot.h5'


def load_model():
    if os.path.exists(model_filename):
        return load_keras_model(model_filename)
    else:
        print("File {} not found!".format(model_filename))
        exit()


def load_image(filename):
    img_arr = img_to_array(load_img(filename, target_size=(224,224)))
    img_arr = img_arr / 255.0
    return np.asarray([img_arr])


def predict(image, model):
    result = model.predict(image)
    noise_image = image - result
    predicted_noise_stdev = np.std(noise_image)
    return predicted_noise_stdev
	
def main(filename):
	keras_model = load_model()
	image = load_image(filename)
	predicted_noise_stdev = predict(image, keras_model)
	return predicted_noise_stdev
	
if __name__ == '__main__':
    filename = sys.argv[1] if len(sys.argv)>=2 else '/images/chip_00001_noise_0.000.png' 
    keras_model = load_model()
    image = load_image(filename)
    predicted_noise_stdev = predict(image, keras_model)
    strng_predicted_noise_stdev = '{:04.3f}'.format(predicted_noise_stdev)
    print("{:30}   {}".format(filename, strng_predicted_noise_stdev))