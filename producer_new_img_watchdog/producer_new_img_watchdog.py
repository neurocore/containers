#producer.py

import time
import sys
from kafka import KafkaProducer

#connect to kafka
producer =KafkaProducer(bootstrap_servers=['kafkazookeeper_kafka_1:9092'])

from watchdog.events import PatternMatchingEventHandler

class MyHandler(PatternMatchingEventHandler):
    patterns = ["*.jpeg", "*.jpg", "*.png", "*.tif"]

    def process(self, event):
		producer.send(topic="get-new-files", value=event.src_path) #, event.event_type)
	# we only care about newly created images
    def on_created(self, event):
        self.process(event)

from watchdog.observers import Observer  
		
if __name__=='__main__':
    args = sys.argv[1:]
    observer = Observer()
    observer.schedule(MyHandler(), path=args[0] if args else '.')
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()
